#!/usr/bin/env nix-shell
#! nix-shell -i bash --pure
#! nix-shell -p bash cacert git idris2

mkdir dependencies
cd dependencies
git clone https://gitlab.com/eleanorofs/idris-fizzbuzz.git
cd idris-fizzbuzz
idris2 --build fizzbuzz.ipkg
idris2 --install fizzbuzz.ipkg
cd ../..
idris2 --build fizzbuzz-main.ipkg
