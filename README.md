# idris-fizzbuzz-main

a FizzBuzz kata demonstrating basic Idris 2 syntax. 

## Dependencies

This project is dependent on 
[idris-fizzbuzz](https://gitlab.com/eleanorofs/idris-fizzbuzz), an example 
library for generating FizzBuzz numbers. 

To install: 

```sh
git clone https://gitlab.com/eleanorofs/idris-fizzbuzz.git
cd idris-fizzbuzz
idris2 --build fizzbuzz.ipkg
idris2 --install fizzbuzz.ipkg
```

## Compilation

After installing the dependency above, the project may be built with: 

```sh
idris2 --build fizzbuzz-main.ipkg
```

A script for compilation including dependency installation is included in 
[compile.sh](https://gitlab.com/eleanorofs/idris-fizzbuzz-main/-/blob/main/compile.sh). 

## Further Reading

This project is the outcome of a blog series on the basics of Idris which 
begins with [Part I: Monads, Comments, and assert_smaller](https://webbureaucrat.gitlab.io/articles/idris-fizzbuzz-part-i-monads-comments-assert-smaller/). 
