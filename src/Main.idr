module Main

import Data.Vect
import FizzBuzz

printFizzBuzz : Vect n Nat -> IO ()
printFizzBuzz Nil = putStrLn ""
printFizzBuzz (x :: xs) = 
  do putStrLn $ FizzBuzz.numberToFBNString x
     printFizzBuzz xs

total
main : IO ()
main = printFizzBuzz $ fromList [ 1 .. 1000 ]
